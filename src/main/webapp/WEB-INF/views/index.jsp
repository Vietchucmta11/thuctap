<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html
PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Trang chủ</title>

<link href="<c:url value="/sources/css/bootstrap.css" />"
	rel="stylesheet">
</head>
<body>
	<div class="wrapper">
		<div class="container">
			<div class="row col-md-12">
				<h2>Bài tập cuối kỳ</h2>
			</div>
			<div class="col-md-12"
				style="border-color: black; border-width: 5px;">
				<div class="col-md-2" style="background: #fff; height: 500px;">
					<div>
						<h4>Nhập vào số điểm</h4>
						<input type="number" max="20" min="0" id="quatity" />
						<button>OK</button>
					</div>
					<div style="height: 50px;"></div>
					<div>
						<div>Nhập vào điểm đầu</div>
						<div class="row">
							<h5 style="width: 100px;">Điểm đầu</h5>
							<input type="text" style="width: 100px;" />
						</div>
						<div class="row">
							<h5 style="width: 100px;">Điểm cuối</h5>
							<input type="text" style="width: 100px;" />
						</div>
						<div style="margin-top: 10px;">
							<button>OK</button>
						</div>
					</div>
				</div>
				<div class="col-md-10" id="chart_div"
					style="background: aqua; height: 490px;"></div>
			</div>
		</div>
	</div>
	<script src="<c:url value="/sources/js/jquery-3.3.1.min.js" />"></script>
	<script src="<c:url value="/sources/js/bootstrap.js" />"></script>
	<script src="<c:url value="/sources/js/creats.js" />"></script>
	<script type="text/javascript"
		src="https://www.gstatic.com/charts/loader.js"></script>
	<script type="text/javascript">
		google.charts.load('current', {
			'packages' : [ 'corechart' ]
		});
		google.charts.setOnLoadCallback(drawChart);

		function drawChart() {
			var data = google.visualization.arrayToDataTable([ [ 'X', 'Y' ],
					[ -8, 12 ], [ 4, 9 ], [ 11, 14 ], [ 40, -5 ], [ 3, 4 ],
					[ 8, 70 ] ]);

			var options = {
				title : '',
				hAxis : {
					title : 'X',
					minValue : -100,
					maxValue : 100
				},
				vAxis : {
					title : 'Y',
					minValue : -100,
					maxValue : 100
				},
				legend : 'none',
				trendlines : {
					0: {
				        labelInLegend: 'Bug line',
				        visibleInLegend: true,
				      }
				}
			};
			var chart = new google.visualization.ScatterChart(document
					.getElementById('chart_div'));

			chart.draw(data, options);
		}
	</script>
</body>
</html>