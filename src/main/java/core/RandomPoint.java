package core;

import java.util.Random;

import model.Point;

public class RandomPoint {
	 
	public Point randomPoint() {
		Point point= new Point();
		Random random= new Random();
		point.setX(random.nextInt(200) -100);
		point.setY(random.nextInt(200) -100);
		return point;
	} 
	
//	public static void main(String[]arg) {
//		Random random= new Random();
//		int x= random.nextInt(2);
//		System.out.println(x);
//	}

}