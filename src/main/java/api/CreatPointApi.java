package api;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import core.RandomPoint;
import model.*;

@RestController
public class CreatPointApi {
	RandomPoint randomPoint = new RandomPoint();

	@RequestMapping(value = "/api/create/{sum}", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public Set<Point> createPoint(@PathVariable(value="sum")int sumPoint) {
		Set<Point> points = new HashSet<>();
		while (points.size() != sumPoint) {
			Point point = new Point();
			point=randomPoint.randomPoint();
			points.add(point);
		}
		return points;
	}
}
